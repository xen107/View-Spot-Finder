package org.example;

import org.example.landscape.Mesh;
import org.json.JSONObject;

import org.junit.Test;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static junit.framework.Assert.assertEquals;

public class TestMain {
    @Test
    public void testMeshElement() throws IOException {
        String json = new String(Files.readAllBytes(Paths.get("src", "test", "resources", "mesh[1][1][1][1][1][1].json")));
        JSONObject jsonObject = new JSONObject(json);
        Mesh mesh = new Mesh(jsonObject).creatMesh();
        int numberOfElements = mesh.getElmCollection().size();
        int elements = jsonObject.getJSONArray("elements").length();
        assertEquals(numberOfElements,elements);
    }


    @Test
    public void testMeshNode() throws IOException {
        String json = new String(Files.readAllBytes(Paths.get("src", "test", "resources", "mesh_x_sin_cos_10000[82][1][1][1][1][1][1].json")));
        JSONObject jsonObject = new JSONObject(json);
        Mesh mesh = new Mesh(jsonObject).creatMesh();
        int numberOfNodes = mesh.getNodeCollection().size();
        int nodes = jsonObject.getJSONArray("nodes").length();
        assertEquals(numberOfNodes,nodes);
    }
    @Test
    public void testMeshValue() throws IOException {
        String json = new String(Files.readAllBytes(Paths.get("src", "test", "resources", "mesh_x_sin_cos_20000[1][1][1][1][1][1](1).json")));
        JSONObject jsonObject = new JSONObject(json);
        Mesh mesh = new Mesh(jsonObject).creatMesh();
        int numberOfValues = mesh.getValueList().size();
        int elements = jsonObject.getJSONArray("values").length();
        assertEquals(numberOfValues,elements);
    }

}
