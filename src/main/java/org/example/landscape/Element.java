package org.example.landscape;

import java.util.ArrayList;

public class Element {
    private int id;
    private ArrayList<Node> nodes;
    private Value scalarValue;

    public Element(int id, ArrayList<Node> nodes, Value scalarValue) {
        this.id = id;
        this.nodes = nodes;
        this.scalarValue = scalarValue;
    }

    public Value getScalarValue() {
        return scalarValue;
    }

    public void setScalarValue(Value scalarValue) {
        this.scalarValue = scalarValue;
    }

    public Element() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public void setNodes(ArrayList<Node> nodes) {
        this.nodes = nodes;
    }
}
