package org.example.landscape;

public class Value {
    private int elmentId;
    private double value;

    public Value(int elmentId, double value) {
        this.elmentId = elmentId;
        this.value = value;
    }

    public Value() {
    }

    public int getElmentId() {
        return elmentId;
    }

    public void setElmentId(int elmentId) {
        this.elmentId = elmentId;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
