package org.example.landscape;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
public class Mesh {
    private HashMap<Integer,Element> elmCollection;
    private HashMap<Integer,Node> nodeCollection;
    private List<Value> valueList;
    private JSONObject meshJson;

    public Mesh(JSONObject meshJson) {
        this.meshJson = meshJson;
        this.elmCollection = new HashMap<>();
        this.nodeCollection = new HashMap<>();
        this.valueList = new ArrayList<>();
    }

    public HashMap<Integer, Element> getElmCollection() {
        return elmCollection;
    }

    public HashMap<Integer, Node> getNodeCollection() {
        return nodeCollection;
    }

    public List<Value> getValueList() {
        return valueList;
    }

    public Mesh creatMesh(){
        log.debug("Creating a MESH ...");
        creatNodeCollection();
        creatValueList();
        creatElementCollection();
        log.debug("mesh has been successfully created");
        return this;
    }
    private void creatNodeCollection(){
        log.debug("Creating a node collection from a json file");
        JSONArray jsonNode  = meshJson.getJSONArray("nodes");

        for(int i = 0;i < jsonNode.length();i++){
            JSONObject tempObj = jsonNode.getJSONObject(i);
            Node tempNode = new Node(tempObj.getInt("id"),tempObj.getDouble("x"),tempObj.getDouble("y"));
            nodeCollection.put(i, tempNode);
        }
    }
    private void creatValueList(){
        log.debug("Creating List of Values from a json file");
        JSONArray jsonValue  = meshJson.getJSONArray("values");

        for(int i = 0;i < jsonValue.length();i++){
            JSONObject tempObj = jsonValue.getJSONObject(i);
            Value tempValue = new Value(tempObj.getInt("element_id"), tempObj.getDouble("value"));
            valueList.add(tempValue);
        }
    }
    private void creatElementCollection(){
        log.debug("Creating Element collection from a json file");
        JSONArray jsonElement  = meshJson.getJSONArray("elements");

        for(int i = 0;i < jsonElement.length();i++){
            JSONObject tempObj = jsonElement.getJSONObject(i);
            JSONArray tempArray = tempObj.getJSONArray("nodes");
            ArrayList<Node> ListOfNodes = new ArrayList<>();

            for(int j = 0;j < tempArray.length();j++){
                ListOfNodes.add(nodeCollection.get(tempArray.getInt(j)));
            }
            int elementId = tempObj.getInt("id");
            Element tempElement = new Element(elementId,ListOfNodes,valueList.get(elementId));

            elmCollection.put(elementId,tempElement);
        }
    }


}
