package org.example.landscape;
public class Node {
    public int id;
    public double xAxis;
    public double yAxis;

    public Node(int id, double xAxis, double yAxis) {
        this.id = id;
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    public Node() {
    }

}
