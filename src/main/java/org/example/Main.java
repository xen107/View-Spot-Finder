package org.example;

import org.json.JSONObject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) throws IOException {
        JSONObject jsonObject = new JSONObject(new String(Files.readAllBytes(Paths.get(args[0]))));
        VSFinder vsFinder = new VSFinder(jsonObject,Integer.parseInt(args[1]));
        vsFinder.printTheViewSpots();
    }
}