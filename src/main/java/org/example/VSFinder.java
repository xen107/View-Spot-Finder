package org.example;

import lombok.extern.slf4j.Slf4j;
import org.example.landscape.Element;
import org.example.landscape.Mesh;
import org.example.landscape.Node;
import org.example.landscape.Value;
import org.json.JSONObject;


import java.util.*;
@Slf4j
public class VSFinder  {
    private Mesh mesh;
    private int firstN;
    private Map<Integer, Element> visitedElements;
    private HashMap<Integer,List<Element>> ListOfSharedPoint;

    public VSFinder(JSONObject meshJson, int firstN) {
        this.mesh = new Mesh(meshJson).creatMesh();
        this.firstN = firstN;
        visitedElements = new HashMap<>();
        ListOfSharedPoint = new HashMap<>();
        creatListOfSharedNode();
    }
    public void printTheViewSpots(){
        ArrayList<JSONObject> viewSpots = new ArrayList<>();
        List<Element> listOViewSpots = findViewSpots();

        for (Element elm : listOViewSpots){
            JSONObject jsonObject = new JSONObject()
                    .put("element_id", elm.getId()).put("value", elm.getScalarValue().getValue());
            viewSpots.add(jsonObject);
        }
        viewSpots.sort(Comparator.comparingDouble(o -> (-(double) o.getDouble("value"))));

        StringBuilder result = new StringBuilder();
        result.append("[\n\t");
        for (JSONObject jsonElement: viewSpots) {
            result.append(jsonElement).append(",").append("\n\t");
        }
        result.delete(result.length() - 3, result.length());
        result.append("\n]");
        System.out.println(result);
    }

    private List<Element> findViewSpots(){
        visitedElements = new HashMap<>();
        List<Element> viewSpots = new ArrayList<>();

        for (Value val: mesh.getValueList()){
            if (viewSpots.size() == firstN){
                return viewSpots;
            }
            Element curElement = mesh.getElmCollection().get(val.getElmentId());

            if(isViewSpot(curElement)){
                viewSpots.add(curElement);
            }
        }
        return viewSpots;
    }

    private boolean isViewSpot(Element element) {
        boolean isViewSpot = true;
        if(visitedElements.containsKey(element.getId())) {
            return isViewSpot= false;
        }
        visitedElements.put(element.getId(), element);

        List<Element> neighbours = new ArrayList<>();
        for (Node node: element.getNodes()) {
            neighbours.addAll(ListOfSharedPoint.get(node.id));
        }

        for(Element neighbourElement : neighbours) {
           if(neighbourElement.getScalarValue().getValue() > element.getScalarValue().getValue()) {
                return isViewSpot=false;
            }else if(neighbourElement.getScalarValue().getValue() == element.getScalarValue().getValue()) {
                if(!visitedElements.containsKey(neighbourElement.getId())) {
                    if (!isViewSpot(neighbourElement)) {
                        return isViewSpot=false;
                    }
                }
            }
        }
        return isViewSpot;
    }

    private void creatListOfSharedNode(){
        log.debug("Create a list of Node with its elements");
        for(int i = 0;i< mesh.getElmCollection().size();i++){
            Element curElm = mesh.getElmCollection().get(i);
            List<Node> nodesOfElm = curElm.getNodes();

            for(Node node: nodesOfElm){
                List<Element> elmList = ListOfSharedPoint.getOrDefault(node.id, new ArrayList<>());
                elmList.add(curElm);
                ListOfSharedPoint.put(node.id, elmList );
            }
        }
    }

    public HashMap<Integer, List<Element>> getListOfSharedPoint() {
        return ListOfSharedPoint;
    }

    public Mesh getMesh() {
        return mesh;
    }
}
